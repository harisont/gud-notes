% Summary of "Learn you a Haskell for great good!"
% Arianna Masciolini

Chapters covered in this summary: 1, 2, 3, 4, 6, 8, 9 (partially).

# Introduction
Haskell is a purely functional programming language. 

In functional programming, you don't tell the computer exactly what to do as such, but rather you tell it what stuff _is_. You express that in the form of __functions__. The only thing a function can do is calculate something and return it as a result (functions have no side-effects), so if a function is called twice with the same parameters, it's guaranteed to return the same result (__*referential transparency*__).

Also, there are __no variables__, but just constants (constant functions).

Haskell is __lazy__, i.e. the evaluation of an expression is delayed until its value is needed. This goes well with referential transparency and it allows to think of programs as a series of transformations on data. It also allows infinite data structures.

Haskell is __statically typed__ (and also __strongly typed__) but has __type inference__ and __polymorphism__.

## Running Haskell programs
To run a program written in Haskell, you can:

- interpret it in (`ghci`) 
- compile it and then run the produced executable file (`ghc --make progrname` + `./progrname`
- use the `runhaskell` command (`runhaskell progrname.hs`)


# Basics

## Arithmetics
Basic arithmetics works as in most other programming languages. A little pitfall to watch out for here is negating numbers: a negative number has to be surrounded with parentheses.

## Functions

Functions __calls__ are composed of the function name, a space and then the parameters, separated by spaces (__no commas between arguments, usually no parentheses__), e.g.

```
max 2 4
```

Functions are usually prefix, but binary functions can also be called as an infix functions by surrounding their name with backticks, so that 
```
4 `div` 2
``` 
is equivalent to `div 4 2`.

In functions __definitions__, quite similarly, the function name is followed by parameters separated by spaces, but there's also an `=`, followed by the body of the function. Example:

```
doubleMe x = x + x 
```

Functions can't begin with uppercase letters, while the character `'` is allowed in function names, and it is usually used to either denote a _strict_ (non-lazy) version of a function or a slightly modified version of an existing function:

```
doubleMe' x = x * 2
```

## Lists
Lists are a __homogenous__ data structure.

List __concatenation__ is done by using the `++` operator (NB: when such operator is used, Haskell has to walk through the whole list on the left side. However, putting something at the beginning of a list using the `:` operator (aka _cons_) is instantaneous.). The __difference between `++` and `:`__ is that the former takes two lists as parameters, while the latter takes a single element and a list. `[1,2,3]` is actually just syntactic sugar for `1:2:3:[]`.

For __indexing__, the `!!` operator is used.

Lists __comparisons__ via the usual operators are allowed (provided that the type of the list is a suitable one). Comparisons happen in lexicographical order: first the heads are compared; if they are equal, the second elements are compared, etc.

### Basic functions operating on lists

- `head` returns the first element of a list
- `tail` returns its tail. In other words, it chops off its head
- `last` returns its last element
- `init` returns everything except its last element
- `length` returns the list's length
- `reverse` reverses it
- `null` checks if it is empty
- `take` extracts a given number of elements from the beginning of the list. If we try to take more elements than there are in the list, it just returns the list. If we try to take 0 elements, we get an empty list
- `drop` works like `take`, but it _drops_ the elements
- `maximum` (resp. `minimum`) returns the biggest (resp. smallest) element in a list (provided that the list can be sorted)
- `sum` and `product` respectively sum and multiplicate the elements of a numeric list
- `elem` returns true if a given element belongs to a given list

### Ranges
Ranges are a way of making lists that are arithmetic sequences of elements that can be enumerated (e.g. `[1..10]`). It is also possible to specify a step. and to make __infinite lists__ by just not specifying an upper limit.
The function `cycle` takes a (finite) list and cycles it into an infinite list, while `repeat` takes a single element and produces an infinite list of just that element.

### Comprehensions
List comprehensions are very similar to set comprehensions, in fact they are normally used for building more specific sets out of general sets. Example:

```
[x*2 | x <- [1..10], x*2 >= 10]
```

## Tuples
Tuples are denoted with parentheses and their components are separated by commas.

They are used when the number of values you to combine is known in advance (e.g. to model multidimensional vectors). Their __type__ depends on how many components it has and the types of the components and can contain a combination of several types.

### Basic functions operating on pairs (2-uples)

- `fst` returns the first component
- `snd` returns the second component
- `zip` produces, given two lists of equal length, a list of pairs.

# Syntax of functions

## Pattern matching
Pattern matching consists of specifying patterns to which some data should conform and then checking to see if it does and deconstructing the data according to those patterns. When defining functions, you can define separate function bodies for different patterns. Example:

```
sayMe :: (Integral a) => a -> String  
sayMe 1 = "One!"  
sayMe 2 = "Two!"  
sayMe 3 = "Three!"  
sayMe 4 = "Four!"  
sayMe 5 = "Five!"  
sayMe x = "Not between 1 and 5" 
```

Note that, if we moved the last pattern (the catch-all one) to the top, it would always say "Not between 1 and 5".

If we define a function and then try to call it with an input that we didn't expect we'll get the following error:

```
"*** Exception: tut.hs:(53,0)-(55,21: Non-exhaustive patterns in function charName
```

That is why we should always include a catch-all pattern so that our program doesn't crash if we get some unexpected input (tough more powerful, pattern matching is to a certain extent similar to `switch` statements, and the catch-all pattern resembles the `default` case).

Example of pattern matching in list comprehensions:

```
ghci> let xs = [(1,3), (4,3), (2,4), (5,3), (5,6), (3,1)]  
ghci> [a+b | (a,b) <- xs]  [4,7,6,8,11,4]
```

Also, lists themselves can be used in pattern matching. You can match with the empty list `[]` or any pattern that involves `:` and the empty list, while a pattern like __`x:xs` will bind the head of the list to `x` and the rest of it to `xs`__, even if there's only one element so xs ends up being an empty list (NB: patterns that have `:` in them only match against lists of length 1 or more).

### "As" patterns
`as` patterns are a handy way of breaking something up according to a pattern and binding it to names whilst still keeping a reference to the whole thing. To do this, an `@` is put in front of a pattern (for instance, `xs@(x:y:ys)` is equivalent, in terms of matching, to `x:y:ys`, but makes it easy to get the whole list via `xs`.

## Guards
Whereas patterns are a way of making sure a value conforms to some form and deconstructing it, guards are a way of testing whether some (boolean) property of a value (or several of them) are true or false (even more similar to switch cases). Guards are indicated by pipes that follow a function's name and its parameters. Many times, the last guard is `otherwise` (cfr. `default`), defined simply as `otherwise = True`.
Example:

```
bmiTell :: (RealFloat a => a -> a -> String
bmiTell weight height      
    | bmi <= skinny = "You're underweight, you emo, you!"
    | bmi <= normal = "You're supposedly normal. Pffft, I bet you're ugly!"
    | bmi <= fat    = "You're fat! Lose some weight, fatty!"
    | otherwise     = "You're a whale, congratulations!"      
    where bmi = weight / height ^ 2 
          skinny = 18.5            
          normal = 25.0            
          fat = 30.0
```

## Where clauses
In the previous example, notice also the use of the keyword `where` after the guards. The names defined in the `where` clause are visible (only) across the guards and give us the advantage of not having to repeat ourselves.

## Let bindings
Very similar to where bindings are `let` bindings. Where bindings are a syntactic construct that let you bind to variables at the end of a function and the whole function can see them, including all the guards. Let bindings let you bind to variables anywhere and are expressions themselves, but are very local, so they don't span across guards.
Their form is let 

```<bindings> in <expression>```

Where the names defined in the `let` part are accessible to the expression after the `in` part.

NB: While `where` bindings are just syntactic constructs, `let` bindings are expressions themselves.

The `in` part can be omitted in lists and when defining functions and constants directly in `GHCi` (in the latter case, the names will be visible throughout the entire interactive session).

## Case expressions
Many imperative languages have case syntax. Haskell takes that concept and one-ups it. The syntax for case expressions is pretty simple:

```
case expression of pattern -> result                     
                   pattern -> result                     
                   pattern -> result                     
                   ...
```

NB: pattern matching on function parameters (in function definitions) is just syntactic sugar for case expressions, but case expressions themselves can be used pretty much anywhere.

# Higher order functions

Haskell functions can take functions as parameters and return functions as return values. A function that does either of those is called a __*higher order function*__.

## Curried functions
Furthermore, every function in Haskell "officially" only takes one parameter; all the functions that accepted several parameters so far have been __*curried functions*__ (cfr. Haskell Curry), meaning that, for example, `max 4 5` first creates a function that takes a parameter and returns either 4 or that parameter, depending on which is bigger. Then, 5 is applied to that function and that function produces our desired result.

Note that putting a space between two things is simply function application: the space is sort of an operator, with the highest precedence.

### Partial application
If we call a function with "too few" parameters, we get back a _partially applied_ function, i.e. a function that takes as many parameters as we left out. Using partial application is a neat way to create functions on the fly, so we can pass them to another function or to seed them with some data.

## Common higher order pre-defined functions
The most commonly used higher order functions in Haskell are `map` and `reduce`:

- The function

    ```
    map :: (a -> b) -> [a] -> [b]  
    map _ [] = []  
    map f (x:xs) = f x : map f xs
    ```

    takes a function and a list and applies that function to every element in the list, producing a new list.

- The function

    ```
    filter :: (a -> Bool) -> [a] -> [a]  
    filter _ [] = []  
    filter p (x:xs)       
        | p x       = x : filter p xs      
        | otherwise = filter p xs
    ```

    takes a _predicate_, i.e. a function that returns a boolean value, and a list and returns the list of elements that satisfy the predicate.

NB: the same results could also be achieved with list comprehensions by the use of predicates.

Other important higher-order functions are folds and scans:

- `foldl` and `foldr`, introduced because of the frequency of usage of the `[x:xs]` pattern in recursive functions dealing with lists, are like `map` function, excepts that they reduce the list to some single value, iterating through it respectively ltr and rtl. A fold takes a binary function, a starting value (the _accumulator_) and a list to fold up

- `foldl1` and `foldr1` work like `foldl` and `foldr`, but take no explicit starting value: they assume the first (resp. last) element of the list to be the starting value and then start the fold with the element next to it. For instance, `sum` can be implemented like 

    ```
    sum = foldl1 (+)
    ```

- `scanl` and `scanr` are also like `foldl` and `foldr`, but they report all the intermediate accumulator states in the form of a list: they are used to monitor the progression of a function that can be implemented as a fold.

### Function application
Another useful higher-order is `$`, aka _function application_, defined as:

```
($) :: (a -> b) -> a -> b  f $ x = f x
```

Whereas normal function application (putting a space between two things) has the highest __precedence__, `$` has the lowest. Function application with a space is left-associative (i.e. `f a b c` is the same as `((f a) b) c`), while function application with `$` is right-associative. 

Most of the time `$`, is a convenience function to avoid writing parentheses, but the meaning of `$` is that __function application can be treated just like another function__. That way, we can, for instance, map function application over a list of functions.


## Lambdas
Lambdas are anonymous functions usually made with the sole purpose of passing them to a higher-order function, using them once only (lambdas are expressions, so we can just pass them like that). Syntax:

```
(\parameters -> body)
```

NB: pattern matching can of course be used also in lambdas, but it's not possible to define multiple parameters for the same parameter.

## Function composition
In Haskell, function composition $f \circ g$ performed via the `.` function, defined as

```
(.) :: (b -> c) -> (a -> b) -> a -> c  
f . g = \x -> f (g x)
```

Function composition is right-associative, so `f (g (z x))` is equivalent to `(f . g . z) x`.

A common use of function composition is defining functions in the so-called __*point free style*__ (aka _pointless_ style):

```
fn x = ceiling (negate (tan (cos (max 50 x))))

fn = ceiling . negate . tan . cos . max 50  --pointless style
``` 


# Pre-defined types and typeclasses

## Types
Good to know:

- explicit types are always denoted with the first letter in capital case
- in GHCi, `:t _expression_` prints out the expression followed by `::` and its type
- When defining a function, explicit type declaration is generally considered to be good practice, despite type inference

### Common types overview

- `Bool`
- `Integer` unbounded integer (arbitrary precision)
- `Int`: bounded integer (more efficient)
- `Float` and `Double`: real floating point numbers
- `Char`
- tuples: they are types as well, but they are dependent on their length and the types of their components, so there is theoretically an infinite number of tuple types (NB: `()` is also a type which can only have a single value: `()`).

### Type variables
If we type `:t head` in GHCi, the result is

```
head :: [a] -> a
```

Note that `a` is not in capital case. If fact, it is not actually a type, but a __*type variable*__, i.e. `a` can be of any type (think about _generics_ in other languages).
Functions that, like `head`, have type variables are called __polymorphic functions__. 

## Typeclasses
A typeclass is a sort of __interface__ that defines some behavior: if a type is a part of a typeclass, that means that it supports and implements the behavior the typeclass describes.

```
ghci> :t (==)  
(==) :: (Eq a) => a -> a -> Bool
```

To be understood, the above example requires some explanations:

- `==`, like any other operator, is a function (NB: if a function is comprised only of special characters, it's considered an infix function by default. If we want to examine its type, pass it to another function or call it as a prefix function, we have to surround it in parentheses)
- Everything before the `=>` symbol is called a __*class constraint*__. So, the above type declaration can be read like this: _the equality function takes any two values that are of the same type and returns a `Bool`. The type of those two values must be a member of the `Eq` class (i.e. the class constraint)_.

### Basic typeclasses overview

- `Eq` is used for types that support equality testing. The functions its members implement are `==` and `/=`
- `Ord` is for types that have an ordering. It covers all the standard comparing functions such as `>`, `<`, `>=` and `<=`. Furthermore, the `compare` function takes two `Ord` members of the same type and returns an ordering. `Ordering` is a type that can be `GT`, `LT` or `EQ` (resp. "greater than", "lesser than" and "equal"
- members of `Show` can be displayed as strings (cfr. `show` function)
- `Read` is sort of the "opposite" typeclass of `Show`. The `read` function takes a string and returns a type which is a member of `Read`, e.g.:

    ```
    ghci> read "True" || False  
    True
    ```
    note that `read "4"` would __not__ work, as GHCI would not know what kind of return value we want (in the previous example, in contrast, we did something with the result afterwards. That way, GHCI could infer what kind of result we wanted out of our read). That's why we can use __explicit type annotations__, by adding `::` at the end of the expression and then specifying a type, e.g. `read "5" :: Int  5`
- `Enum` members are types that can be sequentially enumerated. The main advantage of this typeclass is that we can use its types in __list ranges__
- `Bounded` members have an upper and a lower bound (there are also `minBound` and `maxBound `
- `Num` is a numeric typeclass
- `Integral` is also a numeric typeclass (just for whole numbers). In this typeclass are `Int` and `Integer`
- `Floating` includes only floating point numbers (`Float` and `Double`.

![](venn_typeclasses.png "Relationship between basic typeclasses.")

NB: a very useful function for dealing with numbers is `fromIntegral`. Its type declaration is `fromIntegral :: (Num b, Integral a) => a -> b`: it takes an integral number and turns it into a more general one.

### Functors
The `Functor` typeclass deserves a section of its own as it is a good example of the perks of the non-hierarchical type system, which allows to define and make use of very general, abstract typeclasses.
`Functor` is basically for things that can be mapped over. Its implementation is

```
class Functor f where  
    fmap :: (a -> b) -> f a -> f b  
```

The type of `fmap` is interesting: `f`, in fact, is not a concrete type
but a type constructor that takes one type parameter.

A very common and enlightening example of functors are lists. The function `map`, in fact, is just a `fmap` that works only on lists. Here's how the list is an instance of the `Functor` typeclass:

```
instance Functor [] where      fmap = map
```

Notice how we didn't write `instance Functor [a] where`, because from `fmap :: (a - b) - f a - f b`, we see that the `f` has to be a type constructor that takes one type. `[a]` is already a concrete type (of a list with any type inside it), while `[]` is a type constructor.

Another example of a thing that can be mapped over and made an instance of `Functor` is a `Tree a` type.


# Custom types and typeclasses

## Defining new types
To define a new type, the `data` and `newtype` keywords are used (they only differ in terms of efficiency).
A well known example is

``` 
data Bool = False | True
```

The parts after the `=` are value constructors, which specify the different values that this type can have, while `|` is read as "or". Both the type name and the value constructors have to be capital cased.

Another example is the definition of a type to represent a circle:

```
data Shape = Circle Float Float Float | Rectangle Float Float Float Float

```

Here, the first and second fields are the coordinates of the circle's center and the third field is the radius. By _fields_, we actually mean parameters: value constructors are actually functions that ultimately return a value of a data type. The type signatures for these two value constructors are, in fact:

```
ghci> :t Circle  Circle :: Float -> Float -> Float -> Shape  
ghci> :t Rectangle  Rectangle :: Float -> Float -> Float -> Float -> Shape
```

If, at this stage, we try to just print out `Circle 10 20 5` in the prompt, we'll get an error, as Haskell doesn't know how to display our data type as a string: when we try to print a value out in the prompt, Haskell first runs the `show` function to get the string representation of our value and then it prints that out to the terminal. To make our `Shape` type part of the `Show` typeclass, we add `deriving (Show)` at the end of a data declaration.

If we wanted to export the functions and types that we defined here in a module, we could start it off like this:

```
module Shapes
( Shape(..))
``` 

By doing `Shape(..)`, we exported all the value constructors for `Shape`: it's the same as writing `Shape (Rectangle, Circle)`.

We could also opt not to export any value constructors for Shape by just writing Shape in the export statement. That way, someone importing our module could only make shapes by using the auxiliary functions.

### Record syntax

The syntax seen so far is unreadable in cases such as

```
data Person = Person String String Int Float String String deriving (Show)
```

when there are several fields that can be confused one another. That's when the so-called _record syntax_ comes in handy. Example:

```
data Person = Person
{ firstName :: String,
  lastName :: String, 
  age :: Int,
  height :: Float,
  phoneNumber :: String,flavor :: String 
  } deriving (Show)
```

The resulting data type is exactly the same. The main benefit of this is that it creates functions that lookup fields in the data type. By using record syntax to create this data type, Haskell automatically made these functions: `firstName`, `lastName`, `age`, `height`, `phoneNumber` and `flavor`.
Using this syntax, we don't have to necessarily put the fields in the proper order, as long as we list all of them.

### Type parameters
A value constructor can take some values parameters and then produce a new value. In a similar manner, type constructors can take types as parameters to produce new types. A well known example:

```
data Maybe a = Nothing | Just a  data Maybe a = Nothing | Just a
```

We call `Maybe` a _type constructor_. Depending on what we want this data type to hold when it's not `Nothing`, this type constructor can end up producing a type of `Maybe Int`, `Maybe String`, etc.

Another example of a parameterized type is `Map k v` from `Data.Map`. The `k` is the type of the keys in a map and the `v` is the type of the values. This is a good example of where type parameters are very useful. Having maps parameterized enables us to have mappings from any type to any other type, as long as the type of the key is part of the `Ord` typeclass.

Another cool data type that takes two types as its parameters is the `Either a b type`. It has two value constructors. If the `Left` is used, then its contents are of type `a` and if `Right` is used, then its contents are of type `b`. This is roughly how it's defined:

```
data Either a b = Left a | Right b deriving (Eq, Ord, Read, Show)
```

### Recursive data types
As we've seen, a constructor in an algebraic data type can have several (or none at all) parameters and each of them must be of some _concrete_ type (concrete types are fully applied types like `Maybe Int`). With that in mind, we can make types whose constructors have fields that are of the same type! Using that, we can create recursive data types, where one value of some type contains values of that type, which in turn contain more values of the same type and so on. Let's use algebraic data types to implement our own list:

```
data List a = Empty | Cons a (List a) deriving (Show, Read, Eq, Ord)
```
Here, `Cons` is another word for `:` and it has two fields, one of type `a` and the other of type `[a]`.

Another example defines a tree:
```
data Tree a = EmptyTree | Node a (Tree a) (Tree a) deriving (Show, Read, Eq)
```


### Type synonyms
When writing types, the `[Char]` and `String` types are equivalent and interchangeable. That's implemented with type synonyms. They're just about giving some types different names so that they make more sense to someone reading our code and documentation. Here's how the standard library defines `String` as a synonym for `[Char]`:

``` 
type String = [Char]
```

## Defining and instantiating typeclasses
This is how the Eq class is defined in the standard prelude:

```
class Eq a where
    (==) :: a -> a -> Bool
    (/=) :: a -> a -> Bool
    x == y = not (x /= y)
    x /= y = not (x == y)
```

Here, `class Eq a where` means that we're defining a new typeclass called `Eq`. The `a` is the type variable and it means that a will play the role of the type that we will soon be making an instance of `Eq`. Then, we define several functions. It's not mandatory to implement the function bodies.

Once we have a class, we can start making types instances of that class. The type:

```
data TrafficLight = Red | Yellow | Green  data TrafficLight = Red | Yellow | Green
```

defines the states of a traffic light. Notice how we didn't derive any class instances for it. That's because we're going to write up some instances by hand, even though we could derive them for types like `Eq` and `Show`. Here's how we make it an instance of `Eq`.

```
instance Eq TrafficLight where
    Red == Red = True
    Green == Green = True
    Yellow == Yellow = True
    _ == _ = False
```


# I/O (not exhaustive)
While the fact functions are unable to change the state of a program is mostly, as previously discussed, good, there's one problem with that: how is a function supposed to tell us what it calculated without changing the state of an output device?
Haskell's system for dealing with functions that have __side-effects__ neatly separates the part of our program that is pure and the part of our program that is impure.

Here is a sample program with basic I/O:

```
main = putStrLn "hello, world"
```

Note that the type of `putStrLn` is

```
putStrLn :: String -> IO ()
```

which means that such function takes a string and returns an __I/O action__ that has a result type of `()` (i.e. the empty tuple, also know as __unit__). 
An I/O action is something that, when performed, will carry out an action with a side-effect (usually either reading from the input or printing to the screen) and will also contain some kind of return value inside it. Printing a string to the terminal doesn't really have any kind of meaningful return value, so `()` is used.

## Do blocks
We can use __`do` syntax__ to glue together several I/O actions into one. Example:

```
main = do      
    putStrLn "Hello, what's your name"
    name <- getLine      
    putStrLn ("Hey " ++ name ++ ", you rock!")
```

This reads pretty much like an imperative program: each of the steps inside the `do` is an I/O action, and the result action we got has a type of `IO ()`, because that's the type of the _last_ I/O action inside.

The `main` function always has a type signature of 

```
main :: IO something
```

where `something` is some concrete type. By convention, the type declaration for `main` is usually omitted.

A common I/O function is `getLine :: IO String`: I/O action that contains a result type of String. Performing the I/O action `getLine` and binding its result value to a name (of type `String`) is like putting the input line into a box, and the way to get the data inside it is to use the `<-` construct. In fact, something like

```
nameTag = "Hello, my name is " ++ getLine
```

will not work because he left parameter of `++` has a type of `String`, whilst `getLine` has a type of `IO String`.

It's also important to know that __I/O actions will only be performed when they are given a name of `main` or when they're inside a bigger I/O action that we composed with a `do` block__. Either way, they'll be performed only if they eventually fall into main.

NB: the `return` in Haskell is not like the `return` in imperative languages: there, it usually ends the execution of a method or subroutine and makes it report some sort of value to whoever called it, while in Haskell (in I/O actions specifically), it makes an I/O action out of a pure value. The resulting I/O action doesn't actually do anything, it just has that value encapsulated as its result. The reason why we transform a pure value into an I/O action that doesn't do anything is that we need some I/O action to carry out in any case. In brief, __`return` is the opposite to `<-`__.

## The `when` function
The `when` function is found in `Control.Monad` (to get access to it, do import Control.Monad). In a `do` block, it looks like a control flow statement, but it's actually a normal function. It takes a boolean value and an I/O action. If that boolean value is `True`, it returns the same I/O action that we supplied to it. However, if it's `False`, it returns the `return ()` action.

## Other useful basic I/O functions

- `putStr` 
- `putChar` and `getChar`
- `print` (takes a value of any type that's an instance of `Show`, calls show with that value to stringify it and then outputs that string)
- `forever` (takes an I/O action and returns an I/O action that just repeats the I/O action it got forever)
- `getContents`: an I/O action that reads everything from the standard input until it encounters an end-of-file character. It's useful when we're piping the output from one program into the input of our program, as t does lazy I/O (its result is not represented in memory as a real string, but as a "promise" that it will produce the string eventually

## Some file I/O functions 
A common file I/O function is `openFile` (type signature: `openFile :: FilePath -> IOMode -> IO Handle`). Note that `IOMode` is a type that's defined like this:

```
data IOMode = ReadMode | WriteMode | AppendMode | ReadWriteMode
```

while `FilePath` is just a type synonym for `String`.

Other useful functions are `readFile`, `writeFile` and `appendFile` (self-explanatory).

An even easier way to deal with files is to use the `withFile` function. It takes a path to a file, an `IOMode` and a function that takes a handle and returns some I/O action. What it returns is an I/O action that will open that file, do something we want with the file and then close it. The result encapsulated in the final I/O action that's returned is the same as the result of the I/O action that the function we give it returns.

### Interact
The pattern of getting some string from the input, transforming it with a function and then outputting that is so common that there exists a function which makes that even easier, called `interact`. It takes a function of type `String -> String` as a parameter and returns an I/O action that will take some input, run that function on it and then print out the function's result. 


# Monads
Just read [Functors, applicative and monads in pictures](http://adit.io/posts/2013-04-17-functors,_applicatives,_and_monads_in_pictures.html).
