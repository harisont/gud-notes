# Example questions


## Who has been called the "Grandfather of AI" in the AI literature and why?
As the first philosopher suggesting that thinking might be a mechanical process, some AI researchers have indicated Hobbes as the "grandfather" of AI.

## Give one example of a problem that is better suited for classical AI methods than machine learning and one example of a problem that is better suited for machine learning than classical AI methods.
Despite the popularity of Machine Learning, classical AI methods and ML are both necessary, as each of them is better suited for some classes of problems. A problem that is still better solved via classic search algorithm, for instance, is pathfinding, where the A* algorithm is used. Of course, there are several tasks where Machine Learning techniques has proven to be much more promising. A well known example is image classification, which is accomplished by means of CNNs (Convolutional Neural Networks).

## What is a local receptive field?
The term _local receptive field_ refers to Convolutional NNs, i.e. neural networks specifically designed for image recognition tasks.
In particular, if we imagine the input layer of a CNN as a matrix, a local receptive field is a neuron of a convolutional layer connected to an $n \times n$ submatrix of the input layer. There is one local receptive field per possible $n \times n$ submatrix. 

## Explain how stochastic gradient descent works. 
In _stochastic_ gradient descent, the training set is divided into several random subsets of the same, small size called mini-batches, so that each update step (gradient descent + backprop) is performed on one mini-batch instead of on the entire training set. Such mini-batch, being randomly generated, tends to approximate the whole data set well and, being small, allows faster learning. Once all mini-batches have been used for training, a new _epoch_ begins.

## Give the algorithm k-means clustering in pseudocode.

```
k_means(k)
  randomly initialize the k centroids
  do
    assign each data point to the closest centroid *
    recompute the centroids **
      while (the centroids keep changing ***)
```

\*   according to a given distance metric, e.g. the euclidean distance
**  so that each of them is the "average" of all the points assigned to them 
*** otherwise, for faster convergence, a threshold can be specified

## Explain how autoencoders work.
Autoencoders are feed-forward neural networks used for __unsupervised__ data compression, denoising and representation learning (cfr. Word2Vec).
The simple goal of an autoencoder is to reconstruct the input data as closely as possible. This is not a trivial task, as the inner layers of an autoencoder are much smaller than the input and the output ones. They are composed of an encoder and a decoder: the decoder takes the output of the encoder, i.e. the compressed representation of the data, as its input, and tries to decode it. In most cases, once the desired degree of accuracy is achieved, the encoder alone is used.

## Describe how  $\epsilon$-greedy reinforcement learning algorithms work.
The objective of RL algorithms is to maximise a reward, e.g. the score in a game. In order to do so, they keep track of the outcome of their moves, so to learn from their own experience. To achieve good performance, a RL algorithms alternates "exploitation moves", i.e. moves based on past experiences, and "exploration moves", i.e. random moves, essential to try out new, potentially better strategies that would otherwise left untried. In $\epsilon$-greedy algorithms, in particular, the $\epsilon$ is a real number between 0 and 1 and it refers to how likely it is for an exploratory move to take place: the agent will explore with probability $\epsilon$, and exploit with probability $1-\epsilon$, or vice versa.

## Define accumulated discounted reward.
In the context of RL, the _accumulated reward_ is defined as the reward agent accumulates over time. For example, the accumulated discounted reward from time t on is given by
  $$r_{t} + r_{t+1} + r_{t+2} + ...$$
In many situation, though, the short-term reward is more important than the long-term one. That's why it is useful to introduce a _disocunt factor_ $\gamma$ between 0 and 1 in the above formula: 
  $$r_{t} + r_{t+1} \gamma + r_{t+2} \gamma^{2} + ...$$

## Define the Generic Search Algorithm in pseudocode.

```
gsa(G,s,goal)
  F <- {<s>}
  while (F is not empty)
    select & remove one path p from F *
    if goal(endnode(p) = true)
      return p
    else 
      F <- {p ++ n | n is a child node of n}
  return nothing
```

where:
- `G=(N,A)` is a graph
- `N` is the set of nodes
- `A` is the set of arcs (a binary relation on nodes)
- `s` is the starting node
- `goal(n)` is a function that takes a node as an input and returns true if the node is a goal node, false otherwise
- `F` refers to the frontier
- the symbol `<-` represents assignment 
- the symbol `++` represents path concatenation
- `<...>` denotes a path
Note also that the finite state automata notation is even more appropriate here
\* the selection strategy qualifies the generic algorithm as BFS, DFS etc.

## Define the function that is used in A* for sorting the frontier.
The function $f$ used in A* for sorting the frontier uses both the path cost $c$ and an admissible (i.e. conservative) heuristic function $h$ to estimate the cost from the end node of the path on, so the value of a given path $p$ is
$$f(p) = c(p) + h(p)$$
The path with the minimum $f​$ is selected.

## Explain why a combination of hill-climbing and random exploration might be be more efficient than one of them alone.
Hill climbing on its own has a high risk of remaining trapped in a local minimum, especially for complex, multi-variable functions. That's why it is used together with random exploration.

## Define a genetic programming algorithm for regression (curve-fitting) in detail.
In genetic programming, programs are represented as trees and crossover is defined as creating a child tree by selecting two parent trees and swapping two of their subtrees in the child. Such a technique can be used for curve-fitting, but the study materials do not provide any more specific information.

## Draw a small example of a game tree for a game with 3 players. 
Hehe, not gonna draw anything here. Anyway, the only difficulty of this question is to think of a 3-player game to inspire yourself.

## How do the simulation and backpropagation parts work in Monte Carlo Tree Search?
MCTS is based on simulations. That means that, at each step, several (even random) simulations are run starting from each node of the tree representing the game. After the simulations are run, their value is returned to such node (backpropagation step), so that it is possible to proceed based on the best simulation.

