# GUd-notes 

This repo is a collection of markdown notes for some of the courses I attend during my Master in CS at Göteborgs Universitet. 

- __Parallel computer architecture__ (DIT361GU): extensive __but not complete__ notes. There are some references to specific paragraphs of [this book](https://www.amazon.com/Parallel-Computer-Organization-Design-Michel-ebook-dp-B00FF76R28/dp/B00FF76R28/ref=mt_kindle?_encoding=UTF8&me=&qid=)
- __Functional programming__ (DIT146GU): mostly a summary of part of [this super cool free book](http://learnyouahaskell.com)
- __Introduction to Artificial Intelligence__: answers to the example exam questions.

Note: these notes can to be converted to `.epub` via [pandoc](https://pandoc.org/).
